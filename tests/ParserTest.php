<?php

declare(strict_types=1);

namespace Funpay\Tests;

use Funpay\Parser;
use Funpay\Tokenizer;
use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase
{
    /**
     * @dataProvider smsDataProvider
     *
     * @param string $text
     * @param string[] $expected
     */
    public function testParse(string $text, array $expected)
    {
        $parser = new Parser(new Tokenizer());
        $result = $parser->parse($text);

        self::assertEquals($expected, $result);
    }

    public static function smsDataProvider(): array
    {
        return [
            'р.' => [
                'text' => <<<EOT
Пароль: 5871
Спишется 201,01р.
Перевод на счет 4100151383636
EOT
                ,
                'expected' => ['5871', '201.01', '4100151383636'],
            ],
            'Р.' => [
                'text' => <<<EOT
Пароль: 5871
Спишется 201,01Р.
Перевод на счет 4100151383636
EOT
                ,
                'expected' => ['5871', '201.01', '4100151383636'],
            ],
            'Р' => [
                'text' => <<<EOT
Пароль: 5871
Спишется 201,01Р
Перевод на счет 4100151383636
EOT
                ,
                'expected' => ['5871', '201.01', '4100151383636'],
            ],
            ' руб.' => [
                'text' => <<<EOT
Пароль: 5871
Спишется 201.01 руб.
Перевод на счет 4100151383636
EOT
                ,
                'expected' => ['5871', '201.01', '4100151383636'],
            ],
            ' RUB' => [
                'text' => <<<EOT
Пароль: 5871
Спишется 201.01 RUB
Перевод на счет 4100151383636
EOT
                ,
                'expected' => ['5871', '201.01', '4100151383636'],
            ],
            'RUB' => [
                'text' => <<<EOT
Пароль: 5871
Спишется 201.01RUB
Перевод на счет 4100151383636
EOT
                ,
                'expected' => ['5871', '201.01', '4100151383636'],
            ],
            '200 RUB' => [
                'text' => <<<EOT
Пароль: 5871
Спишется 200 RUB
Перевод на счет 4100151383636
EOT
                ,
                'expected' => ['5871', '200', '4100151383636'],
            ],
            'Кошелёк. Код подтверждения: 1611' => [
                'text' => 'Кошелёк. Код подтверждения: 1611',
                'expected' => ['1611', null, null],
            ],
            'Для входа в систему используйте код 750124. Код действителен в течение 5 мин' => [
                'text' => 'Для входа в систему используйте код 750124. Код действителен в течение 5 мин',
                'expected' => ['750124', null, null],
            ],
            'Kod dlya vhoda: 3532' => [
                'text' => 'Kod dlya vhoda: 3532. Nikomu ne soobschayte etot kod',
                'expected' => ['3532', null, null],
            ],
            'Your confirmation code is: 333729. Your code expires in 5 minutes. Please don\'t reply.' => [
                'text' => 'Your confirmation code is: 333729. Your code expires in 5 minutes. Please don\'t reply.',
                'expected' => ['333729', null, null],
            ],
        ];
    }
}