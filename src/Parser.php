<?php

declare(strict_types=1);

namespace Funpay;

class Parser
{
    /**
     * @var Tokenizer
     */
    private $tokenizer;

    public function __construct(Tokenizer $tokenizer)
    {
        $this->tokenizer = $tokenizer;
    }

    public function parse(string $text): array
    {
        $code = null;
        $account = null;
        $amount = null;

        $tokens = $this->tokenizer->tokenize($text);

        $index = 0;
        do {
            $token = $tokens[$index];

            switch ($token->getType()) {
                case Token::ACCOUNT:
                    $account = (string) $token;
                    break;
                case Token::MONEY:
                    $amount = (string) $token;
                    break;
                case Token::FLOAT:
                case Token::INTEGER:
                case Token::CODE;
                    $nextToken = $tokens[$index + 1] ?? null;
                    if (null !== $nextToken && $nextToken->isType(Token::ROUBLES)) {
                        $amount = (string)$token;
                        $index++;
                    }
                    break;
                case Token::CODE_WORD:
                    // search for number after code word in next 5 word
                    for ($i = $index + 1; $i < $index + 5 && isset($tokens[$i]); $i++) {
                        if ($tokens[$i]->isType(Token::CODE)) {
                            $code = (string) $tokens[$i];
                            $index = $i;
                            break;
                        }
                    }
                    break;
            }

            $index++;
        } while (isset($tokens[$index]));

        return [$code, $amount, $account];
    }
}