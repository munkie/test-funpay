<?php

declare(strict_types=1);

namespace Funpay;

class Tokenizer
{
    const CODE_WORDS = ['пароль', 'код', 'parol', 'parol\'', 'kod', 'code'];

    const PUNCTS = '.,:-;';

    public function createToken(string $word): Token
    {
        $trimmed = trim($word, self::PUNCTS);

        if (in_array(mb_strtolower($trimmed), self::CODE_WORDS)) {
            return new Token(Token::CODE_WORD, $trimmed);
        }

        $matches = [];

        if (1 === preg_match('/^\d{11,16}$/u', $trimmed)) {
            return new Token(Token::ACCOUNT, $trimmed);
        }

        if (1 === preg_match('/^(\d+)([\.,](\d{2}))?(р|руб|rub)$/iu', $trimmed, $matches)) {
            return new Token(Token::MONEY, $matches[1].'.'.$matches[3]);
        }

        if (1 === preg_match('/^\d{1,3}$/u', $trimmed)) {
            return new Token(Token::INTEGER, $trimmed);
        }

        if (1 === preg_match('/^\d{4,6}$/u', $trimmed)) {
            return new Token(Token::CODE, $trimmed);
        }

        if (1 === preg_match('/^(\d+)[\.,](\d{2})$/iu', $trimmed, $matches)) {
            return new Token(Token::FLOAT, $matches[1].'.'.$matches[2]);
        }

        if (1 === preg_match('/^(р|руб|rub)$/iu', $trimmed, $matches)) {
            return new Token(Token::ROUBLES, $trimmed);
        }

        return new Token(Token::WORD, $trimmed);
    }

    /**
     * @param string $text
     * @return Token[]
     */
    public function tokenize(string $text): array
    {
        $words = preg_split('/\s+/u', $text);

        return array_map(
            function ($word): Token {
                return $this->createToken($word);
            },
            $words
        );
    }
}