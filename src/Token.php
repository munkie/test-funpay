<?php

declare(strict_types=1);

namespace Funpay;

class Token
{
    const CODE_WORD = 'code_word';
    const CODE = 'code';
    const INTEGER = 'integer';
    const FLOAT = 'float';
    const MONEY = 'money';
    const ROUBLES = 'roubles';
    const ACCOUNT = 'account';
    const WORD = 'word';

    /** @var string */
    private $type;

    /** @var string */
    private $text;

    public function __construct(string $type, string $text)
    {
        $this->type = $type;
        $this->text = $text;
    }

    public function isType(string $type): bool
    {
        return $this->type === $type;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function __toString(): string
    {
        return $this->getText();
    }
}